import React from 'react';
import './Cart.css';

const Cart = props => {
    const sum = props.order.reduce((acc, meal) => acc + (meal.amount * meal.price), 0);
    return (
        <div className="cart">
            {props.order.map((meal, key) => {
                return (
                    meal.amount > 0 ? <div key={key} onClick={() => props.remove(meal.name)}>{meal.name} x{meal.amount} <span
                        className="price">{meal.price * meal.amount} KGS</span></div> : null
                )
            })}
            <p>Доставка: 150 сом</p>
            <p>Итого: {sum > 0 ? sum + 150 : 0} сом</p>
            <button className={sum > 0 ? "enabled" : "disabled"} onClick={props.show}>Place order</button>
        </div>
    )
};

export default Cart;