import React from 'react';
import './Modal.css';

const Modal = (props) => {
    return (
        <div className="modal">
            <label><span>Ваше имя:</span>
                <input onChange={props.nameChange} value={props.user.name} type="text"/>
            </label><br/>
            <label><span>Ваш e-mail:</span>
                <input type="text" onChange={props.mailChange} value={props.user.mail}/>
            </label><br/>
            <label><span>Ваш телефон:</span>
                <input type="text" onChange={props.phoneChange} value={props.user.phoneNumber}/>
            </label>
            <button onClick={props.hide}>Создать заказ</button>
        </div>
    )
};

export default Modal;