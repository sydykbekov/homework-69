import React from 'react';
import './MealsMenu.css';

const MealsMenu = props => {
    return (
        <div className="menu">
            {props.meals.map((meal, key) => {
                return (
                    <div key={key} className="meal-container">
                        <img src={meal.url} alt=""/>
                        <h3>{meal.name}</h3>
                        <span>{meal.price} сом</span>
                        <button onClick={() => props.add(meal.name)}>Add to cart</button>
                    </div>
                )
            })}
        </div>
    )
};

export default MealsMenu;