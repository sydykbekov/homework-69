import axios from 'axios';
export const GET_MEALS_SUCCESS = 'GET_MEALS_SUCCESS';
export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';

export const getMealsSuccess = meals => {
    return {type: GET_MEALS_SUCCESS, value: meals};
};

export const startRequest = () => {
    return {type: START_REQUEST};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const successRequest = () => {
    return {type: SUCCESS_REQUEST};
};

export const getMeals = () => {
    return (dispatch) => {
        dispatch(startRequest());
        axios.get('meals.json').then(response => {
            const meals = [];
            for (let key in response.data) {
                meals.push(response.data[key]);
            }
            dispatch(getMealsSuccess(meals));
            dispatch(successRequest());
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const sendOrder = (user) => {
    return (dispatch, getState) => {
        dispatch(startRequest());
        const cart = getState().cart.cart.map(meal => meal.amount > 0 ? {[meal.name]: meal.amount} : null);
        const info = {user, cart};
        axios.post('rest-orders.json', info).then(() => {
            dispatch(successRequest());
        }, error => {
            dispatch(errorRequest());
        })
    };
};