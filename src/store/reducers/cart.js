import {GET_MEALS_SUCCESS} from "../actions/actions";
const initialState = {
    cart: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MEALS_SUCCESS:
            const newState = {...state, cart: [...state.cart]};
            newState.cart = action.value.map(meal => ({name: meal.name, price: meal.price, amount: 0}));
            return newState;
        case 'ADD':
            const index = state.cart.findIndex(meal => meal.name === action.value);
            const cart = [...state.cart];
            cart[index].amount++;
            return {...state, cart};
        case 'REMOVE':
            const indexForRemove = state.cart.findIndex(meal => meal.name === action.value);
            const updatedCart = [...state.cart];
            updatedCart[indexForRemove].amount = 0;
            return {...state, cart: updatedCart};
        default:
            return state;
    }

};

export default reducer;