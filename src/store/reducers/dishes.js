import {GET_MEALS_SUCCESS, START_REQUEST, ERROR_REQUEST, SUCCESS_REQUEST} from '../actions/actions';

const initialState = {
    meals: [],
    modal: false,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MEALS_SUCCESS:
            return {...state, meals: action.value};
        case 'MODAL SHOW':
            return {...state, modal: true};
        case START_REQUEST:
            return {...state, modal: false, loading: true};
        case ERROR_REQUEST:
            return {...state, loading: false};
        case SUCCESS_REQUEST:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;