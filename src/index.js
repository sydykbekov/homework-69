import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from "axios";
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import combineReducers from "redux/es/combineReducers";
import dishesReducer from './store/reducers/dishes';
import cartReducer from './store/reducers/cart';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    dishes: dishesReducer,
    cart: cartReducer
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

axios.defaults.baseURL = 'https://react-burger-15fc4.firebaseio.com/';

ReactDOM.render(<Provider store={store}><App /></Provider>  , document.getElementById('root'));
registerServiceWorker();