import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import MealsMenu from '../../components/MealsMenu/MealsMenu';
import './Restaurant.css';
import {getMeals, sendOrder} from "../../store/actions/actions";
import Cart from "../../components/Cart/Cart";
import Modal from "../../components/Modal/Modal";
import preloader from '../../assets/Preloader.gif';

class Restaurant extends Component {
    state = {
        user: {
            name: '',
            mail: '',
            phoneNumber: ''
        }
    };

    changeName = (event) => {
        console.log(this.state);
        const user = {...this.state.user};
        user.name = event.target.value;
        this.setState({user});
    };

    changeMail = (event) => {
        const user = {...this.state.user};
        user.mail = event.target.value;
        this.setState({user});
    };

    changePhone = (event) => {
        const user = {...this.state.user};
        user.phoneNumber = event.target.value;
        this.setState({user});
    };

    componentDidMount() {
        this.props.getMeals();
    }

    render() {
        return (
            <Fragment>
                <img className="preloader" style={{display: this.props.loading ? 'block' : 'none'}} src={preloader} alt="preloader"/>
                {this.props.modal ?
                    <Modal hide={() => this.props.sendOrder(this.state.user)} user={this.state.user} nameChange={this.changeName}
                           mailChange={this.changeMail} phoneChange={this.changePhone}/> : null}
                <div className="container">
                    <MealsMenu meals={this.props.meals} add={this.props.addMeal}/>
                    <Cart remove={this.props.removeMeal} order={this.props.cart} show={this.props.modalShow}/>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        meals: state.dishes.meals,
        cart: state.cart.cart,
        modal: state.dishes.modal,
        loading: state.dishes.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getMeals: () => dispatch(getMeals()),
        addMeal: (name) => dispatch({type: 'ADD', value: name}),
        removeMeal: (name) => dispatch({type: 'REMOVE', value: name}),
        modalShow: () => dispatch({type: 'MODAL SHOW'}),
        sendOrder: (user) => dispatch(sendOrder(user))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Restaurant);