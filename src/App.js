import React, {Component} from 'react';
import Restaurant from './containers/Restaurant/Restaurant';

class App extends Component {
    render() {
        return <Restaurant />
    }
}

export default App;
